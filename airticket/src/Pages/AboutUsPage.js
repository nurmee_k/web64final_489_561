import AboutUs from "../components/AboutUs";

function AboutUsPage() {

    return(
        <div>
            <div align="center">
                <h2> รายละเอียดการจอง </h2>
                
                <AboutUs name="นูรมี กลูแป"
                         flight="Bangkok - Chiangmai"
                         date="Sat 02-Apr-2022"
                         time="10:00 - 10:50" />
                
            </div>

        </div>

    );
}

export default AboutUsPage;