
import TotalResult from "../components/TotalResult";
import { useState } from "react";

import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import { Typography, Box, Container } from "@mui/material";

function TotalCalPage() {

    const [ name, setName ] = useState("");
    const [ totalResult, setTotalResult ] = useState("0");
    const [ translateResult, setTranslateResult ] = useState("");

    const [ flight, setFlight] = useState("");
    const [ date, setDate] = useState("");
    const [ time, setTime] = useState("");

    const [ seat, setSeat ] = useState("");
    const [ price, setPrice ] = useState("");

    function calculateTotal() {
        let s = parseInt(seat);
        let p = parseInt(price)
        let total = (s * p);
        setTotalResult(total);
        if (total > 1200) {
            setTranslateResult("ได้รับส่วนลด20%")
        }else {
            setTranslateResult("ไม่ได้รับส่วนลดนี้")
        }
        
    }
    return(
        <Container sx={{ width:"1g" }} >
            <Grid container spacing={2} sx={{ marginTop: "10px"}}>
                <Grid item xs={12}>
                    <Typography variant="h5">
                        ยินดีต้อนรับสู่เว็บจองตั๋วเครื่องบิน
                    </Typography>
                </Grid>
                <Grid item xs={8}>
                    <Box sx={{ textAlign : 'left' }}>
                        ชื่อ-สกุล: <input type="text" 
                                        value={name} 
                                        onChange={ (e) => {setName(e.target.value); } } /> 
                                        <br />
                                        <br />

                        ไฟล์ทบิน: <input type="text" 
                                        value={flight} 
                                        onChange={ (e) => {setFlight(e.target.value); } } /> 
                                        <br />
                                        <br />

                        วันที่: <input type="text" 
                                        value={date} 
                                        onChange={ (e) => {setDate(e.target.value); } } /> 
                                        <br />
                                        <br />

                        เวลา: <input type="text" 
                                        value={time} 
                                        onChange={ (e) => {setTime(e.target.value); } } /> 
                                        <br />
                                        <br />

                        จำนวนที่นั่ง: <input type="text" 
                                        value={seat} 
                                        onChange={ (e) => {setSeat(e.target.value); } } /> 
                                        <br />
                                        <br />

                        ราคา: <input type="text" 
                                        value={price} 
                                        onChange={ (e) => {setPrice(e.target.value); } } /> 
                                        <br />
                                        <br />

                        <Button variant="contained" onClick={ ()=>{ calculateTotal() } } >
                            ยืนยัน
                        </Button>
                    </Box>
                </Grid>
                <Grid item xs={4}>
                    { totalResult !=0 &&
                        <div>
                        <hr />
                            ยืนยันการจองเรียบร้อยแล้ว

                            <TotalResult
                                name = { name }
                                flight = { flight }
                                date = { date }
                                time = { time }
                                total = { totalResult}
                                seat = { seat }
                                price = { price }
                                result = { translateResult}
                        />
                        </div>
                    }           
                </Grid>
            </Grid>
        </Container>
    );
}
export default TotalCalPage;

