import logo from './logo.svg';
import './App.css';

import AboutUsPage from './Pages/AboutUsPage';
import TotalCalPage from './Pages/TotalCalPage';
import Header from './components/Header';

import { Routes, Route } from "react-router-dom"
import { Details } from '@mui/icons-material';

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
          <Route path="about" element={
                      <AboutUsPage />   
                } />

          <Route path="/" element={
                   <TotalCalPage />
                } />
      </Routes>
       
    </div>
  );
}

export default App;
