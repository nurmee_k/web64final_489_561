import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';

function AboutUs (props) {

    return (
        <Box sx={{ width: "60%" }}>
            <Paper elevation={3}>
                <h3>การจองของฉัน </h3>
                <h4> ชื่อ-นามสกุล: { props.name } </h4>
                <h4> Flight: { props.flight } </h4>
                <h4> Date: { props.date } </h4>
                <h4> Time: { props.time } </h4>
            </Paper>

            <Paper elevation={3}>
                <h3>โปรตั๋วราคาดี</h3>
                <h4>เมษายน - กันยายน</h4>
                <h2> 599.- </h2>
                <h4>
                    เมื่อจองตั๋วเครื่องบินครบ  1,200 บาท <br />
                    รับส่วนลดทันที 20% 
                </h4>

            </Paper>

            

        </Box>
    )
}

export default AboutUs;