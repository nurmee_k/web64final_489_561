function TotalResult (props) {

    return(
        <div>
            <h3> ชื่อ-นามสกุล : {props.name} </h3>
            <h3> ไฟล์ทบิน: {props.flight} </h3>
            <h3> วันที่: {props.date} </h3>
            <h3> เวลา: {props.time} </h3>
            <h3> จำนวนที่นั่ง: {props.seat} </h3>
            <h3> ราคา: {props.price} </h3>
            <h3> รวมทั้งหมด: {props.total} </h3>
            <h3> โปรโมชั่นส่วนลด: {props.result} </h3>
        </div>
    );
}

export default TotalResult;