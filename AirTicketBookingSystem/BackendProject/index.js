const bcrypt =require('bcrypt')
const SALT_ROUNDS = 10

const mysql = require('mysql')

const jwt = require('jsonwebtoken')

const dotenv = require('dotenv')
dotenv.config()
const TOKEN_SECRET = process.env.TOKEN_SECRET

const connection = mysql.createConnection({
    host : 'localhost',
    user : 'ticket',
    password : 'ticket',
    database : 'AirTicketBookingSystem'
});

connection.connect()

const express = require ('express')
const app = express()
const port = 4000



/* Middleware for Authenticating User Token */

function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader && authHeader.split(' ')[1]

    if (token == null) return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, user) => {
        if (err) { return res.sendStatus(403) }
        else {
            req.user = user
            next()
        }
    })
}

/*
Query 1 : List all Flight

SELECT Passenger.PassengerID, Passenger.Name, Passenger.Surname, 
       Location.Country, Location.City, Flight.TravelDates, Flight.Airline
FROM Passenger, Location, Flight
WHERE (Flight.PassengerID = Passenger.PassengerID) AND 
      (Flight.LocationID = Location.LocationID); */

      app.get("/list_registrations", (req, res) =>{
        let query = `
        SELECT Passenger.PassengerID, Passenger.Name, Passenger.Surname, 
               Location.Country, Location.City, Flight.TravelDates, Flight.Airline

        FROM Passenger, Location, Flight

        WHERE (Flight.PassengerID = Passenger.PassengerID) AND 
              (Flight.LocationID = Location.LocationID);`;
              
        connection.query( query, (err, rows) => {
            if (err) {
                res.json({
                            "status" : "400",
                            "message" : "Error querying from running db"
                        })
            }else {
                res.json(rows)
            }
        });
    })


/*
Query 2 : List all Flight by Location

SELECT Passenger.PassengerID, Passenger.Name, Passenger.Surname, 
       Location.Country, Location.City, Flight.TravelDates, Flight.Airline
FROM Passenger, Location, Flight
WHERE (Flight.PassengerID = Passenger.PassengerID) AND 
      (Flight.LocationID = Location.LocationID) AND
      (Location.LocationID = 1) */

      app.get("/list_reg_location", (req, res) =>{

        let location_id = req.query.location_id

        let query = `
        SELECT Passenger.PassengerID, Passenger.Name, Passenger.Surname, 
               Location.Country, Location.City, Flight.TravelDates, Flight.Airline
        FROM Passenger, Location, Flight
        WHERE (Flight.PassengerID = Passenger.PassengerID) AND 
              (Flight.LocationID = Location.LocationID) AND
              (Location.LocationID = ${location_id})`;
              
        connection.query( query, (err, rows) => {
            if (err) {
                res.json({
                            "status" : "400",
                            "message" : "Error querying from running db"
                        })
            }else {
                res.json(rows)
            }
        });
    })


/*
Query 3 : List all location register by passenger ID

SELECT Passenger.PassengerID, Location.Country, Location.City, Flight.TravelDates, Flight.Airline
FROM Passenger, Location, Flight
WHERE (Passenger.PassengerID = Flight.PassengerID) AND 
	  (Flight.LocationID = Location.LocationID) AND
      (Passenger.PassengerID = 1)
*/

app.get("/list_reg_bypassengerid", (req, res) =>{

    let passenger_id = req.query.passenger_id

    let query = `
    SELECT Passenger.PassengerID, Location.Country, Location.City, 
           Flight.TravelDates, Flight.Airline
    FROM Passenger, Location, Flight
    WHERE (Passenger.PassengerID = Flight.PassengerID) AND 
          (Flight.LocationID = Location.LocationID) AND
          (Passenger.PassengerID  = ${passenger_id});`;
          
    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error querying from running db"
                    })
        }else {
            res.json(rows)
        }
    });
})

/*API for Registering a new Location*/
app.post("/register_location", authenticateToken, (req, res) => {
    let user_profile = req.user

    let passenger_id = req.user.user_id
    let location_id = req.query.location_id
    let traveldates = req.query.traveldates
    let airline = req.query.airline

    let query = `INSERT INTO Flight
                    (PassengerID, LocationID, TravelDates, Airline)
                    VALUES ('${passenger_id}','${location_id}',
                            '${traveldates}',
                            '${airline}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                        "status" : "200",
                        "message" : "Adding Flight succesful"
            })
        }
    });
});

/* API for Processing Passenger Authorization */
app.post("/login", (req, res) => {

    let username = req.query.username
    let user_password = req.query.password
    let query = `SELECT * FROM Passenger WHERE Username='${username}'`

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error querying from passenging db"
                    })
        }else {
            let db_password = rows[0].Password
            bcrypt.compare(user_password, db_password, (err, result) => {
                if (result) {
                    let payload = {
                        "name" : rows[0].Name,
                        "surname" : rows[0].Surname,
                        "username" : rows[0].Username,
                        "user_id" : rows[0].PassengerID
                    }
                    console.log(payload)
                    let token = jwt.sign(payload, TOKEN_SECRET, {expiresIn : '1d' })
                    res.send(token)
                }else { res.send("Invalid username / password") }

            })
        }
    })
})


/*API for Registering a new Passenger*/
app.post("/register_passenger", (req, res) => {

    let name = req.query.name
    let surname = req.query.surname
    let telephone_number = req.query.telephone_number
    let email = req.query.email
    let address = req.query.address
    let username = req.query.username
    let password = req.query.password

    bcrypt.hash(password, SALT_ROUNDS, (err, hash) => {
    
           let query = `INSERT INTO Passenger
                        (Name, Surname, TelephoneNumber, Email, Address, Username, Password)
                         VALUES ('${name}','${surname}',
                                 '${telephone_number}',
                                 '${email}','${address}',
                                 '${username}','${hash}')`
            console.log(query)

            connection.query( query, (err, rows) => {
            if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
            }else {
            res.json({
                        "status" : "200",
                        "message" : "Adding new user succesful"
                    })
                }
            });
        })
});



/* CRUD Operation for Location Table */
app.get("/list_location", (req, res) =>{
    let query = "SELECT * FROM Location";
    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error querying from running db"
                    })
        }else {
            res.json(rows)
        }
    });
})

app.post("/add_location", (req, res) => {

    let country = req.query.country
    let city = req.query.city

    let query = `INSERT INTO Location
                    (Country, City)
                    VALUES ('${country}','${city}')`
    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            res.json({
                        "status" : "400",
                        "message" : "Error inserting data into db"
                    })
        }else {
            res.json({
                        "status" : "200",
                        "message" : "Adding location succesful"
            })
        }
    });
})

app.post("/update_location", (req, res) => {
    
    let location_id = req.query.location_id
    let country = req.query.country
    let city = req.query.city

    let query = `UPDATE Location SET
                    Country='${country}',
                    City='${city}'
                    WHERE LocationID=${location_id}`

    console.log(query)

    connection.query( query, (err, rows) => {
        if (err) {
            console.log(err)
            res.json({
                        "status" : "400",
                        "message" : "Error updating record"
                    })
        }else {
            res.json({
                        "status" : "200",
                        "message" : "Updating location succesful"
            })
        }
    });
})


app.post("/delete_location" , (req, res) => {

    let location_id = req.query.location_id

    let query = `DELETE FROM Location WHERE LocationID=${location_id}`

     console.log(query)           
     
     connection.query( query, (err, rows) => {
         if(err){
             console.log(err)
             res.json({
                        "status" : "400",
                        "message": "Error deleting record"
                     })
             }else{
                 res.json({
                     "status" : "200",
                     "message": "Deleting record success"  
              })
           }
      });
 
  })

app.listen(port, () => {
    console.log(` Now starting AirTicketBooking System Backend ${port} `)
})
