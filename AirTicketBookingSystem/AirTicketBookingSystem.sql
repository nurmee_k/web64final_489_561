-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: mariadb
-- Generation Time: Apr 03, 2022 at 02:19 PM
-- Server version: 10.7.3-MariaDB-1:10.7.3+maria~focal
-- PHP Version: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `AirTicketBookingSystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `Flight`
--

CREATE TABLE `Flight` (
  `FlightID` int(11) NOT NULL,
  `PassengerID` int(11) NOT NULL,
  `LocationID` int(11) NOT NULL,
  `TravelDates` date NOT NULL,
  `Airline` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Flight`
--

INSERT INTO `Flight` (`FlightID`, `PassengerID`, `LocationID`, `TravelDates`, `Airline`) VALUES
(1, 1, 1, '2022-04-30', 'Jetstar Asia'),
(2, 2, 2, '2022-04-27', 'Thai Lion Air'),
(3, 2, 2, '2022-04-27', 'Thai Lion Air'),
(4, 5, 2, '2022-06-26', 'Jetstar Asia'),
(5, 5, 1, '2023-07-22', 'Qantas'),
(6, 5, 2, '2023-07-25', 'Qantas'),
(7, 2, 2, '2022-04-29', 'Thai Lion Air'),
(8, 7, 1, '2025-10-28', 'Thai Lion Air');

-- --------------------------------------------------------

--
-- Table structure for table `Location`
--

CREATE TABLE `Location` (
  `LocationID` int(11) NOT NULL,
  `Country` varchar(100) NOT NULL,
  `City` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Location`
--

INSERT INTO `Location` (`LocationID`, `Country`, `City`) VALUES
(1, 'France', 'Paris'),
(2, 'Thailand', 'Chiangmai'),
(3, 'Japan', 'Akita'),
(6, 'Aaloborg', 'Denmark'),
(9, 'Thailand', 'Bangkok');

-- --------------------------------------------------------

--
-- Table structure for table `Passenger`
--

CREATE TABLE `Passenger` (
  `PassengerID` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Surname` varchar(50) NOT NULL,
  `TelephoneNumber` int(11) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Address` varchar(300) NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Password` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Passenger`
--

INSERT INTO `Passenger` (`PassengerID`, `Name`, `Surname`, `TelephoneNumber`, `Email`, `Address`, `Username`, `Password`) VALUES
(1, 'Eman', 'Chinarong', 622336655, 'eman123@gmail.com', '49/1 ม.11 ต.บอ อ.ยอ จ.สงขลา ', '', ''),
(2, 'Nurmee', 'Kalupae', 655562233, 'nurmee123@gmail.com', '141 ม.1 ต.กอ อ.ขอ จ.สงขลา', '', ''),
(3, 'Chatnapa', 'Pai', 985672343, 'pai123@gmail.com', '123 ม.2 ต.ตอ อ.หอ จ.สงขลา', '', ''),
(5, 'Eman', 'Chinarong', 622336655, 'eman123@gmail.com', '49/1 ม. 119 ต.บอ อ.ยอ จ.สงขลา', 'Eman.a', '$2b$10$D.c2ryBajqBe64MoAkeHBeRf3XT4NV8LOF3F5cAugws30slFWmjYW'),
(6, 'Eman', 'Chinarong', 622336655, 'eman123@gmail.com', '49/1 ม. 119 ต.บอ อ.ยอ จ.สงขลา', 'Eman.p', '$2b$10$cHbBZIfHi/CxnaWEw43gs.WRkPxQ0Sv8UnbKrTC0mPxhNeVWry07S'),
(7, 'Eman', 'Chinarong', 622336655, 'eman123@gmail.com', '49/1 ม. 119 ต.บอ อ.ยอ จ.สงขลา', 'Eman.t', '$2b$10$EjoE66sM.Fm2cm1jPq8bgu9NkLgbl8wj7h7bCWmejuP98v3PlqeUm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Flight`
--
ALTER TABLE `Flight`
  ADD PRIMARY KEY (`FlightID`),
  ADD KEY `PassengerID` (`PassengerID`),
  ADD KEY `LocationID` (`LocationID`);

--
-- Indexes for table `Location`
--
ALTER TABLE `Location`
  ADD PRIMARY KEY (`LocationID`);

--
-- Indexes for table `Passenger`
--
ALTER TABLE `Passenger`
  ADD PRIMARY KEY (`PassengerID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Flight`
--
ALTER TABLE `Flight`
  MODIFY `FlightID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `Location`
--
ALTER TABLE `Location`
  MODIFY `LocationID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `Passenger`
--
ALTER TABLE `Passenger`
  MODIFY `PassengerID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Flight`
--
ALTER TABLE `Flight`
  ADD CONSTRAINT `LocationID` FOREIGN KEY (`LocationID`) REFERENCES `Location` (`LocationID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `PassengerID` FOREIGN KEY (`PassengerID`) REFERENCES `Passenger` (`PassengerID`) ON DELETE NO ACTION ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
